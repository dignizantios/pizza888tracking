//
//  UIColor+Extension.swift
//  PizzaTracking
//
//  Created by Jaydeep on 23/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {    
    
    static var appThemeBlackColor: UIColor { return UIColor.init(red: 41/255, green: 38/255, blue: 39/255, alpha: 1.0) }
    static var appThemeLightGrayColor: UIColor { return UIColor.init(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0) }
    static var appThemeDarkGrayColor: UIColor { return UIColor.init(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0) }
    static var appThemeGreenColor: UIColor { return UIColor.init(red: 15/255, green: 201/255, blue: 90/255, alpha: 1.0) }
    static var appThemeRedColor: UIColor { return UIColor.init(red: 236/255, green: 28/255, blue: 35/255, alpha: 1.0) }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
