//
//  Double+Extension.swift
//  PizzaTracking
//
//  Created by Jaydeep on 02/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}
