//
//  UIViewController+Extension.swift
//  PizzaTracking
//
//  Created by Jaydeep on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import FirebaseDatabase

extension UIViewController {
    
    func navigateUserWithLG(obj : UIViewController)
    {
        let navigationController = NavigationController(rootViewController: obj)
        
        let mainViewController = MainViewController()
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 6)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
    }
    
    func setUpNavigationBarWithTitle(strTitle : String) {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.3
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeRedColor
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done_key".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .appThemeRedColor
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    @objc func btnBackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl,isWidth:Bool = true)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .bottom
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        if isWidth{
            dropdown.width = sender.frame.size.width
        } else {
            dropdown.bottomOffset = CGPoint(x: -40, y: sender.bounds.height)
        }
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    //MARK: - Logout API
    
    func logoutAPICalling() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBaseURL)\(kLogout)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "business_id" : getUserDetail("business_id"),
                         "device_token" : UUID().uuidString,
                         "driver_id":getUserDetail("driver_id")
            ]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        
                        var ref =  DatabaseReference()
                        ref = Database.database().reference()
                        ref.child("user-\(getUserDetail("driver_id"))").removeValue()
                        
                        Defaults.removeObject(forKey: "userDetails")
                        Defaults.synchronize()
                        AppDelegate.shared.locationManager.stopUpdatingLocation()
                        Defaults.removeObject(forKey: "isTripStarted")
                        Defaults.synchronize()
                        
                        let vc  = objStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigateUserWithLG(obj:vc)
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
}
