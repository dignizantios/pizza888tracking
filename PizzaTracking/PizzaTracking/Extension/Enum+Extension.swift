//
//  Enum+Extension.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

enum selectionParentControlFromSidemenu{
    case home
    case mytrip
}

enum selectJobType:String {
    case simpleJob = "0"
    case thirdPartyJob = "1"
}

enum selectFinishFlag:String {
    case finish = "0"
    case autoFinish = "1"
}

enum selectPaymentType:String {
    case paid = "1"
    case unPaid = "2"
}

enum selectedPaymentFlag:String {
    case cash = "1"
    case card = "2"
}


