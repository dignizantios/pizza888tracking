//
//  HomeParentModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeParentModel: NSObject {
    
    fileprivate weak var theController:HomeParentVC!
    
    //MARK:- LifeCycle
    
    init(theController:HomeParentVC) {        
        self.theController = theController
    }

    //MARK:- Variable Declaration
    
    var selectedParentController = selectionParentControlFromSidemenu.home
}
