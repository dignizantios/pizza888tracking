//
//  HomeParentView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import CarbonKit

class HomeParentView: UIView {
    
    //MARK:-Variable Declaration
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = [String]()
    
    //MARK:- Outlet Zone
    @IBOutlet weak var vwMain: UIView!
    
    //MARK:- Setup UI
    
    func setupUI(theDelegate:HomeParentVC,selectionVC:selectionParentControlFromSidemenu){
        
        if selectionVC == .home {
            items = ["My_jobs_key".localized.localizedCapitalized,"Third_party_jobs_key".localized.localizedCapitalized]
        } else {
            items = ["My_trip_key".localized.localizedCapitalized,"Third_party_jobs_key".localized.localizedCapitalized]
        }
         
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: theDelegate)
        carbonTabSwipeNavigation.insert(intoRootViewController: theDelegate, andTargetView: vwMain)
        
        style()
    }
    
    func style() {
        let width = UIScreen.main.bounds.size.width
        
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        
        let tabWidth = (width / CGFloat(items.count))
        let indicatorcolor: UIColor = UIColor.appThemeRedColor
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.appThemeLightGrayColor, font: themeFont(size: 17, fontname: .regular))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.appThemeRedColor, font: themeFont(size: 17, fontname: .semibold))
    }
       
}
