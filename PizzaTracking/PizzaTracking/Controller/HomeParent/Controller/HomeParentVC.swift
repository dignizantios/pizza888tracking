//
//  HomeParentVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import CarbonKit

class HomeParentVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: HomeParentView = { [unowned self] in
        return self.view as! HomeParentView
        }()
    
    lazy var theCurrentModel: HomeParentModel = {
        return HomeParentModel(theController: self)
    }()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.setupUI(theDelegate: self, selectionVC: theCurrentModel.selectedParentController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = false
        if theCurrentModel.selectedParentController == .home {
            setUpNavigationBarWithTitle(strTitle: "Home_key".localized)
        } else {
            setUpNavigationBarWithTitle(strTitle: "My_trip_key".localized.capitalized)
        }
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu"), style: .plain, target: self, action: #selector(showLeftViewAnimated(_:)))
        leftButton.tintColor = UIColor.appThemeRedColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
    }
    
    func setupData(selecedVC:selectionParentControlFromSidemenu){
        theCurrentModel.selectedParentController = selecedVC
    }

}

//MARK:- CarbonKit delgate

extension HomeParentVC:CarbonTabSwipeNavigationDelegate{
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch index {
        case 0:
            let vc = objStoryboard.instantiateViewController(withIdentifier: "HomeChildVC") as! HomeChildVC
            vc.setupModelData(type: .simpleJob, parentType: theCurrentModel.selectedParentController)
            return vc
        case 1:
            let vc = objStoryboard.instantiateViewController(withIdentifier: "ThirdPartyListVC") as! ThirdPartyListVC
            vc.setupModelData(type: .thirdPartyJob,parentType: theCurrentModel.selectedParentController)
            return vc
           
        default:
            let vc = objStoryboard.instantiateViewController(withIdentifier: "HomeChildVC") as! HomeChildVC
            vc.setupModelData(type: .simpleJob, parentType: theCurrentModel.selectedParentController)
            return vc
        }
    }
}
