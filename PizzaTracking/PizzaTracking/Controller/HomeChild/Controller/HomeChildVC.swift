//
//  HomeChildVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeChildVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: HomeChildView = { [unowned self] in
        return self.view as! HomeChildView
        }()
       
    var upperReferesh = UIRefreshControl()
    
    let homeViewModel = HomeChildViewModel()
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setupUI(theDelegate: self)
        setupUI()
        if homeViewModel.selectMainParentController == .home {
            APICallingFromHome()
        } else {
           APICallingFromMyTrip()
        }
    }
    
    func setupModelData(type:selectJobType,parentType:selectionParentControlFromSidemenu){
        homeViewModel.selectedParentCotroller = type
        homeViewModel.selectMainParentController = parentType
    }
    
    func APICallingFromHome(isLoaderShow:Bool = true){
        if isLoaderShow {
            showLoader()
            homeViewModel.intOffset = 0
        }
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(homeViewModel.intOffset)",
                    "flag":"\(homeViewModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token")]
        
        homeViewModel.getJobListData(kURL: kJobs, param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                if self.homeViewModel.intOffset == 0 || self.homeViewModel.intOffset == -1  {
                    self.homeViewModel.arrJobList = []
                }
                self.homeViewModel.dictHomeList = list
                self.homeViewModel.arrJobList += list.data
                self.mainView.tblHome.reloadData()
            } else {
                self.homeViewModel.dictHomeList = list
                self.mainView.tblHome.reloadData()
            }
        }
    }
    
    @objc func APICallForHome(){
        APICallingFromHome()
    }
    
    func APICallingFromMyTrip(isLoaderShow:Bool = true){
        if isLoaderShow {
            showLoader()
            homeViewModel.intOffset = 0
        }
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(homeViewModel.intOffset)",
                    "flag":"\(homeViewModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token")]
        
        homeViewModel.getJobListData(kURL: kMyTrip, param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                if self.homeViewModel.intOffset == 0 || self.homeViewModel.intOffset == -1 {
                    self.homeViewModel.arrJobList = []
                }
                self.homeViewModel.dictHomeList = list
                self.homeViewModel.arrJobList += list.data
                self.mainView.tblHome.reloadData()
            } else {
                self.homeViewModel.dictHomeList = list
                self.mainView.tblHome.reloadData()
            }
        }
    }
    
    @objc func APICallForTrip(){
        APICallingFromMyTrip()
    }
    
    func setupUI(){
        if homeViewModel.selectMainParentController == .home {
            upperReferesh.addTarget(self, action: #selector(APICallForHome), for: .valueChanged)
        } else {
           upperReferesh.addTarget(self, action: #selector(APICallForTrip), for: .valueChanged)
        }
        
        mainView.tblHome.addSubview(upperReferesh)
        mainView.tblHome.tableFooterView = UIView()
    }
    
}

//MARK:- TableViewDelegate

extension HomeChildVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if homeViewModel.arrJobList.count == 0 {
            let lbl = UILabel()
            lbl.text = homeViewModel.dictHomeList.msg
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return homeViewModel.arrJobList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        let dict = homeViewModel.arrJobList[indexPath.row]
        cell.lblOrderId.text = "\("Order_id_key".localized) : \(dict.orderNum)"
        cell.lblName.text = "\(dict.custFname) \(dict.custLname)"
        cell.lblStoreName.text = "\(dict.storeName)"
        cell.lblAddress.text = "\(dict.custAddress)"
        cell.lblPaymentName.text = "\(dict.paymentType)"
        cell.lblAmount.text = "$ \(dict.orderPrice)"
        cell.btnStartOutlet.tag = indexPath.row
        cell.btnStartOutlet.addTarget(self, action: #selector(btnStartAction(_:)), for: .touchUpInside)
        if homeViewModel.selectMainParentController == .mytrip {
            cell.viewStart.isHidden = true
        }
        cell.vwPaymentFlag.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if homeViewModel.selectMainParentController == .mytrip {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyTripVC") as! MyTripVC
            obj.setupDataJobData(theModel: homeViewModel.arrJobList[indexPath.row], type: homeViewModel.selectMainParentController, jobType: homeViewModel.selectedParentCotroller)
            obj.handlerUpdateList = {[weak self] dict,isCancel in
                if isCancel {
                    self?.removeFromList(dict: dict)
                } else {
                   self?.updateHomeList(dict: dict)
                }                
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && homeViewModel.intOffset != -1 && homeViewModel.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    func updateHomeList(dict:JobModelData){
        if let index = homeViewModel.arrJobList.firstIndex(where: {$0.ordid == dict.ordid}) {
            homeViewModel.arrJobList[index] = dict
            UIView.performWithoutAnimation {
                self.mainView.tblHome.reloadData()
            }
        }
    }
    
    func removeFromList(dict:JobModelData){
        if let index = homeViewModel.arrJobList.firstIndex(where: {$0.ordid == dict.ordid}) {
            homeViewModel.arrJobList.remove(at: index)
            UIView.performWithoutAnimation {
                self.mainView.tblHome.reloadData()
            }
        }
    }
    
    @objc func btnStartAction(_ sender:UIButton){
        let dictData = homeViewModel.arrJobList[sender.tag]
        let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                "ordid":dictData.ordid,
                                "driver_id":getUserDetail("driver_id"),
                                "access_token":getUserDetail("access_token"),
                                "flag":homeViewModel.selectedParentCotroller.rawValue,
                                "lang":lang]
        
        homeViewModel.startJob(param: dict) { (data) in
            if data.flag  == Int(strAccessDenied){
                self.logoutAPICalling()
            } else if data.flag == Int(strSuccessResponse){
                makeToast(strMessage: data.msg)
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyTripVC") as! MyTripVC
                obj.setupDataJobData(theModel: data.data[0], type: .mytrip, jobType: self.homeViewModel.selectedParentCotroller)
                if self.homeViewModel.selectMainParentController == .mytrip {
                    obj.handlerUpdateList = {[weak self] dict,isCancel in
                        if isCancel {
                           self?.updateHomeList(dict: dict)
                        } else {
                            self?.removeFromList(dict: dict)
                        }
                    }
                } else {
                    self.removeFromList(dict: data.data[0])
                }
                self.navigationController?.pushViewController(obj, animated: true)
                self.toRedirectGoogleMaps(model: data.data[0])
            }
        }
    }
    
    func toRedirectGoogleMaps(model:JobModelData){
        if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(model.lattitude),\(model.longitude)&directionsmode=driving") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
}

//MARK:- Scrollview delegate

extension HomeChildVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if homeViewModel.intOffset != -1 && homeViewModel.intOffset != 0 {
            if (Double(mainView.tblHome.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(mainView.tblHome.contentSize.height).rounded(toPlaces: 2)) - (Double(mainView.tblHome.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                if homeViewModel.selectMainParentController == .home {
                    APICallingFromHome(isLoaderShow: false)
                } else {
                   APICallingFromMyTrip(isLoaderShow: false)
                }
            }
        }
    }
}
