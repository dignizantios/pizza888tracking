import Foundation 
import ObjectMapper 

class JobModelList: Mappable { 

	var nextOffset: String = ""
	var flag: Int = 0
	var msg: String = ""
	var data = [JobModelData]()

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		nextOffset <- map["next_offset"] 
		flag <- map["flag"] 
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class JobModelData: Mappable {

	var ordid: String = ""
	var orderNum: String = ""
	var paidStatus: String = ""
	var paymentType: String = ""
	var orderStatus: String = ""
	var orderPrice: String = ""
	var custFname: String = ""
	var custLname: String = ""
	var custMobile: String = ""
	var custAddress: String = ""
	var unitNum: String = ""
	var custStreetnum: String = ""
	var custStreet: String = ""
    var lattitude: Double = 0.0
	var longitude: Double = 0.0
    var storeName: String = ""
    var orderTrackId:String = ""
    var polyline : String = ""
    
	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		ordid <- map["ordid"] 
		orderNum <- map["order_num"] 
		paidStatus <- map["paid_status"] 
		paymentType <- map["payment_type"] 
		orderStatus <- map["order_status"] 
		orderPrice <- map["order_price"] 
		custFname <- map["cust_fname"] 
		custLname <- map["cust_lname"] 
		custMobile <- map["cust_mobile"] 
		custAddress <- map["cust_address"] 
		unitNum <- map["unit_num"] 
		custStreetnum <- map["cust_streetnum"] 
		custStreet <- map["cust_street"] 
		lattitude <- map["lattitude"] 
		longitude <- map["longitude"]
        storeName <- map["store_name"]
        orderTrackId <- map["order_track_id"]
        polyline <- map["polyline"]
	}
} 

