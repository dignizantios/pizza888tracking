//
//  HomeChildView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeChildView: UIView {
    
    //MARK:- Outlet Zone

    @IBOutlet weak var tblHome: UITableView!
    
   
    
    //MARK:- ViewLife Cycle
    
    func setupUI(theDelegate:HomeChildVC){
        
        tblHome.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        
        
    }

}
