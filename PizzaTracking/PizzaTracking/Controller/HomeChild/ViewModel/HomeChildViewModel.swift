//
//  HomeChildViewModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 02/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class HomeChildViewModel {
    
    //MARK:- Variable Declaration
    
    var arrJobList:[JobModelData] = []
    var intOffset:Int = 0
    var selectedParentCotroller = selectJobType.simpleJob
    var dictHomeList = JobModelList()
    var selectMainParentController = selectionParentControlFromSidemenu.home
    
    //MARK:- Initilization
    
    init() {
        
    }
}

//MARK:- Service

extension HomeChildViewModel {
    
    func getJobListData(kURL:String,param:[String:Any],completion:@escaping(JobModelList) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kURL)"
            
            print("URL: \(url)")
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let jobList = JobModelList(JSON: dicData) {
                            self.intOffset = json["next_offset"].intValue
                            completion(jobList)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let jobList = JobModelList(JSON: dicData) {                            
                            completion(jobList)
                        }
                    }
                    else {
                        if let dicData =  json.dictionaryObject , let jobList = JobModelList(JSON: dicData) {
                            self.intOffset = json["next_offset"].intValue
                            completion(jobList)
                        }
                        self.intOffset = json["next_offset"].intValue
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
    
    func startJob(param:[String:Any],completion:@escaping(JobModelList) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kStartJob)"
            
            print("URL: \(url)")
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = JobModelList(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = JobModelList(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
}
