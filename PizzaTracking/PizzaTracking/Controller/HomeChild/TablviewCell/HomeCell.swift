//
//  HomeCell.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var lblPaymentName: UILabel!
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnStartOutlet: CustomButton!
    @IBOutlet weak var viewStart: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewOrderStore: UIView!
    @IBOutlet weak var vwPaymentFlag: UIView!
    @IBOutlet weak var btnCashOutlet: CustomButton!
    @IBOutlet weak var btnCardOutlet: CustomButton!
    @IBOutlet weak var lblPaymentFlagTitle: UILabel!
    @IBOutlet weak var vwPaymentType: UIView!
    
    //MARK:- ViewLifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblOrderId,lblAmount,lblAmountTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = .appThemeGreenColor
        }
        
        
        [lblName,lblNameTitle,lblStoreTitle,lblStoreName,lblAddress,lblAddressTitle,lblPaymentTitle,lblPaymentName,lblPaymentFlagTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = .appThemeBlackColor
        }
        
        lblNameTitle.text = "\("Name_key".localized) : "
        lblStoreTitle.text = "\("Order_store_key".localized.capitalized) : "
        lblAddressTitle.text = "\("Address_key".localized.capitalized) : "
        lblPaymentTitle.text = "\("Payment_type_key".localized.capitalized) : "
        lblAmountTitle.text = "\("Amount_key".localized.capitalized) : "
        lblOrderId.text = "Order_id_key".localized
        lblPaymentFlagTitle.text = "\("Payment_type_key".localized.capitalized) : "
        
        [btnStartOutlet,btnCardOutlet,btnCashOutlet].forEach { (btn) in
            btn?.backgroundColor = .appThemeRedColor
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .regular)
            btn?.setTitleColor(.white, for: .normal)
        }
       
        btnStartOutlet.setTitle("Start_key".localized, for: .normal)
        btnCashOutlet.setTitle("Cash_key".localized, for: .normal)
        btnCardOutlet.setTitle("Card_key".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
