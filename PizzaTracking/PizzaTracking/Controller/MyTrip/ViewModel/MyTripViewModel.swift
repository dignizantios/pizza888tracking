//
//  MyTripViewModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 03/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class MyTripViewModel {
    
    //MARK:- Variable Declaration
    
    var dictJobData = JobModelData()
    var selectMainParentController = selectionParentControlFromSidemenu.home
    var selectedParentCotroller = selectJobType.simpleJob
    var arrWayPoint: [CLLocationCoordinate2D] = []
    var strFinalPolyline:String = ""
    var dictThirdPartyJobData = ThirdPartyJobListData()

}

//MARK:- ViewLife Cycle

extension MyTripViewModel {
    
    func finishJob(param:[String:Any],completion:@escaping(ThirdPartyAddedData) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kFinishJob)"
            
            print("URL: \(url)")
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
}

