//
//  MyTripView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps

class MyTripView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var lblPaymentName: UILabel!
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    @IBOutlet weak var btnFinishOutlet: UIButton!
    @IBOutlet weak var vwBottom: CustomView!
    @IBOutlet weak var vwGoogleMaps: GMSMapView!
    @IBOutlet weak var vwName: UIView!
    @IBOutlet weak var vwOrderStore: UIView!
    
    //MARK:- Setup UI
    
    func setupUI(theDelegate:MyTripVC) {
        [lblOrderId,lblAmount,lblAmountTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = .appThemeGreenColor
        }
        [lblName,lblNameTitle,lblStoreTitle,lblStoreName,lblAddress,lblAddressTitle,lblPaymentTitle,lblPaymentName].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .regular)
            lbl?.textColor = .appThemeBlackColor
        }
        
        lblNameTitle.text = "\("Name_key".localized) : "
        lblStoreTitle.text = "\("Order_store_key".localized.capitalized) : "
        lblAddressTitle.text = "\("Address_key".localized.capitalized) : "
        lblPaymentTitle.text = "\("Payment_type_key".localized.capitalized) :"
        lblAmountTitle.text = "\("Amount_key".localized.capitalized) : "
        
        [btnCancelOutlet,btnFinishOutlet].forEach { (btn) in
            btn?.setTitleColor(.black, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .regular)
        }
        
        btnCancelOutlet.setTitle("Cancel_key".localized, for: .normal)
        btnFinishOutlet.setTitle("Finish_key".localized, for: .normal)
        
        if theDelegate.model.selectMainParentController == .home {
            vwBottom.isHidden =  true
        }
    }
    
    func setupData(theModel:JobModelData){
        lblOrderId.text = "\("Order_id_key".localized) : \(theModel.orderNum)"
        lblName.text = "\(theModel.custFname) \(theModel.custLname)"
        lblStoreName.text = "\(theModel.storeName)"
        lblAddress.text = "\(theModel.custAddress)"
        lblPaymentName.text = "\(theModel.paymentType)"
        lblAmount.text = "$ \(theModel.orderPrice)"
    }
    
    func setupData(theModel:ThirdPartyJobListData){
        [vwOrderStore,vwName].forEach { (vw) in
            vw?.isHidden = true
        }
        lblOrderId.text = "\("Order_id_key".localized) : \(theModel.orderId)"
        lblPaymentName.text = theModel.paymentStatus == "1" ? "Cash_key".localized : "Card_key".localized
        lblAddress.text = theModel.deliveryAddress
        lblAmount.text = "$ \(theModel.orderPrice)"
    }

}
