//
//  MyTripVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Polyline
import CoreLocation
import GoogleMaps
import FirebaseDatabase

class MyTripVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: MyTripView = { [unowned self] in
        return self.view as! MyTripView
        }()
    
    let model = MyTripViewModel()
    var handlerUpdateList:(JobModelData,Bool) -> Void = {_,_ in }
    var handlerThirdPartyUpdateList:(ThirdPartyJobListData,Bool) -> Void = {_,_ in }
    var ref =  DatabaseReference()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()

        mainView.setupUI(theDelegate: self)
        
        kOrdId = self.model.selectedParentCotroller == .simpleJob ? self.model.dictJobData.ordid : self.model.dictThirdPartyJobData.ordid
        kJobFlag = self.model.selectedParentCotroller.rawValue
        kOrderTrackId = self.model.selectedParentCotroller == .simpleJob ? self.model.dictJobData.orderTrackId : self.model.dictThirdPartyJobData.orderTrackId
        
        kDeliverLatitude = self.model.selectedParentCotroller == .simpleJob ? Double(self.model.dictJobData.lattitude) :  Double(self.model.dictThirdPartyJobData.deliveryLat)!
        kDeliverLongitude = self.model.selectedParentCotroller == .simpleJob ? Double(self.model.dictJobData.longitude) : Double(self.model.dictThirdPartyJobData.deliveryLong)!
        
        if model.selectedParentCotroller == .simpleJob {
            if model.dictJobData.orderStatus == "8" {
                Defaults.setValue("1", forKey: "isTripStarted")
                Defaults.synchronize()
                AppDelegate.shared.setUpQuickLocationUpdate()
            } else if model.dictJobData.orderStatus == "5" || model.dictJobData.orderStatus == "6" {
                drawPath(strPolyLine: model.dictJobData.polyline)
                mainView.vwBottom.isHidden = true
            }
            mainView.setupData(theModel: model.dictJobData)
        } else {
            if model.dictThirdPartyJobData.orderStatus == "8" {
                Defaults.setValue("1", forKey: "isTripStarted")
                Defaults.synchronize()
                AppDelegate.shared.setUpQuickLocationUpdate()
            } else if model.dictThirdPartyJobData.orderStatus == "5" || model.dictThirdPartyJobData.orderStatus == "6" {
                drawPath(strPolyLine: model.dictThirdPartyJobData.polyline)
                mainView.vwBottom.isHidden = true
            }
            mainView.setupData(theModel: model.dictThirdPartyJobData)
        }
              
        AppDelegate.shared.handlerLocationUpdate = {[weak self] lattitude,longitude,polyLine in
            guard self != nil else {
                return
            }
            if self?.model.selectedParentCotroller == .simpleJob {
                if self?.model.dictJobData.orderStatus == "8" {
                    self?.model.strFinalPolyline = polyLine
                    self?.drawPath(strPolyLine: polyLine)
                }
            } else {
                if self?.model.dictThirdPartyJobData.orderStatus == "8" {
                    self?.model.strFinalPolyline = polyLine
                    self?.drawPath(strPolyLine: polyLine)
                }
            }
        }
        
        AppDelegate.shared.handlerBackToMainView = {[weak self] in
            self?.navigationController?.popViewController(animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: "My_trip_key".localized.capitalized)
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: .plain, target: self, action: #selector(btnBackAction))
        leftButton.tintColor = UIColor.appThemeRedColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
    }
    
    func setupDataJobData(theModel:JobModelData,type:selectionParentControlFromSidemenu,jobType:selectJobType) {
        model.dictJobData = theModel
        model.selectMainParentController = type
        model.selectedParentCotroller = jobType
    }
    
    func setupDataThirdParty(theModel:    ThirdPartyJobListData,type:selectionParentControlFromSidemenu,jobType:selectJobType) {
        model.dictThirdPartyJobData = theModel
        model.selectMainParentController = type
        model.selectedParentCotroller = jobType
    }
}

//MARK:- Action Zone

extension MyTripVC {
    
    @IBAction func btnCancelAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CancelTripVC") as! CancelTripVC
        obj.handlerUpdateList = {[weak self] in
            AppDelegate.shared.locationManager.stopUpdatingLocation()
            Defaults.removeObject(forKey: "isTripStarted")
            Defaults.synchronize()
            self?.ref.child("user-\(getUserDetail("driver_id"))").removeValue()
            if self?.model.selectedParentCotroller == .simpleJob {
                self?.handlerUpdateList(self!.model.dictJobData, true)
            } else {
                self?.handlerThirdPartyUpdateList(self!.model.dictThirdPartyJobData, true)
            }
            self?.navigationController?.popViewController(animated: true)
        }
        if self.model.selectedParentCotroller == .simpleJob {
            obj.setupDataJobData(theModel: model.dictJobData, type: .simpleJob)
        } else {
            obj.setupDataThirdPartyData(theModel: model.dictThirdPartyJobData, jobType: .thirdPartyJob)
        }
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)        
    }
    
    @IBAction func btnFinishAction(_ sender:UIButton){
        self.view.endEditing(true)
        alertForFinish()
    }
    
    func alertForFinish(){
        let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_finish_this_job_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                     "ordid":self.model.selectedParentCotroller == .simpleJob ? self.model.dictJobData.ordid : self.model.dictThirdPartyJobData.ordid,
                                     "finish_flag":"0",
                                     "driver_id":getUserDetail("driver_id"),
                                     "access_token":getUserDetail("access_token"),
                                     "lang":lang,
                                     "flag":self.model.selectedParentCotroller.rawValue,
                                     "order_track_id":self.model.selectedParentCotroller == .simpleJob ? self.model.dictJobData.orderTrackId : self.model.dictThirdPartyJobData.orderTrackId,
                                     "polyline":self.model.strFinalPolyline]
            
            self.model.finishJob(param: dict) { (data) in
                if data.flag  == Int(strAccessDenied){
                    self.logoutAPICalling()
                } else if data.flag == Int(strSuccessResponse) {
                    self.ref.child("user-\(getUserDetail("driver_id"))").removeValue()
                    AppDelegate.shared.locationManager.stopUpdatingLocation()
                    Defaults.removeObject(forKey: "isTripStarted")
                    Defaults.synchronize()
                    if self.model.selectedParentCotroller == .simpleJob {
                        self.handlerUpdateList(self.model.dictJobData, false)
                    } else {                        self.handlerThirdPartyUpdateList(self.model.dictThirdPartyJobData,false)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK:- Create Path

extension MyTripVC {
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)  {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 1 {
            marker.icon = UIImage.init(named: "ic_star_pick_up_location")
        } else if type == 2 {
            let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
//            self.mainView.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18)
            let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: Float(googleMapZoomLevel))
            self.mainView.vwGoogleMaps.animate(with: updateCamera)
            marker.icon = UIImage.init(named: "ic_map_pin")
        }
        marker.map = mainView.vwGoogleMaps
    }
    
    func drawPath(strPolyLine:String) {
        self.mainView.vwGoogleMaps.clear()
        let polyline = Polyline(encodedPolyline:strPolyLine)
        let decodedCoordinates: [CLLocationCoordinate2D] = polyline.coordinates!

        let path = GMSMutablePath()
        for i in 0..<decodedCoordinates.count {
            let dictFirst = decodedCoordinates[i]
            path.add(CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude))
            if i == 0 {
                 self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 1)
            } else if i == ((decodedCoordinates.count) - 1) {
                self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 2)
                /*if self.isRideStarted == false
                {
                    let target = CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude)
                    self.mainView.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: target, zoom: 18)
                }*/
            }
        }
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 2
        rectangle.strokeColor = .appThemeRedColor
        rectangle.map = self.mainView.vwGoogleMaps
        if model.selectedParentCotroller == .simpleJob {
            if model.dictJobData.orderStatus == "5" || model.dictJobData.orderStatus == "6" {
//                fitAllMarkers(_path: path)
            }
        } else {
            if model.dictThirdPartyJobData.orderStatus == "5" || model.dictThirdPartyJobData.orderStatus == "6" {
//              fitAllMarkers(_path: path)
            }
        }
    }
    
    func fitAllMarkers(_path: GMSPath) {
        var bounds = GMSCoordinateBounds()
        for index in 1..._path.count() {
            bounds = bounds.includingCoordinate(_path.coordinate(at: index))
        }
        mainView.vwGoogleMaps.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20))
    }
}
