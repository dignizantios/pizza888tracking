//
//  LoginViewModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 30/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class LoginViewModel {
    
    //MARK:- Variable Declaration
    
    var arrBusinessList:[SearchBusiness] = []
    var strBusinessId = String()
    
    //MARK:- Initilization
    
    init() {
        
    }
}

//MARK:- Service

extension LoginViewModel {
    
    func getBusinessListData(keyword:String,completion:@escaping(String,[SearchBusiness]) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kSearchBusinessList)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "search_keyword" : keyword,
                         "business_id":"1"]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
               
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let searchBusinessList = SearchBusinessList(JSON: dicData) {
                            completion(json["flag"].stringValue,searchBusinessList.data)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
    
    func postLoginData(param:[String:Any],completion:@escaping(String,loginData,JSON) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kLogin)"
            
            print("URL: \(url)")

            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
               
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let loginUser = LoginModel(JSON: dicData) {
                            completion(json["flag"].stringValue,loginUser.loginUserData, json["data"])
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
    
}
