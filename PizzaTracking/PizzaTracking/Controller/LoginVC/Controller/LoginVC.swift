//
//  LoginVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 23/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import DropDown

class LoginVC: UIViewController {
    
    //MARK: Variables    
    
    lazy var mainView: LoginView = { [unowned self] in
        return self.view as! LoginView
        }()
    
    let loginViewModel = LoginViewModel()
    var businessDD = DropDown()
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.setupUI(theDelegate: self)
        
        mainView.txtSearchBusiness.addTarget(self, action: #selector(textFiedlDidChange(_:)), for: .editingChanged)
        APICalling()
        
        if getUserDetail("driver_id") != "" {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "HomeParentVC") as! HomeParentVC
            self.navigateUserWithLG(obj: obj)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = true
    }
    
    override func viewDidLayoutSubviews() {
        self.configureDropdown(dropdown: businessDD, sender: mainView.txtSearchBusiness)
    }
    
    func APICalling() {
        loginViewModel.getBusinessListData(keyword: "") { (flg, list) in
            self.loginViewModel.arrBusinessList = list
        }
    }

}

//MARK:- UITextfied Delegate

extension LoginVC:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        mainView.unSelectedSetup()
        if textField == mainView.txtSearchBusiness{
            mainView.selectedSetup(label:mainView.lblSearchTitle,textfield: mainView.txtSearchBusiness)
            self.businessDD.dataSource = loginViewModel.arrBusinessList.map{($0.businessName)}
            self.businessDD.show()
            self.businessDD.selectionAction = { (index: Int, item: String) in
                self.mainView.txtSearchBusiness.text = item
                self.loginViewModel.strBusinessId = self.loginViewModel.arrBusinessList[index].businessId
            }
        } else if textField == mainView.txtUsername {
            mainView.selectedSetup(label:mainView.lblUsernameTitle,textfield: mainView.txtUsername)
        } else if textField == mainView.txtPassword {
            mainView.selectedSetup(label: mainView.lblPasswordTitle, textfield: mainView.txtPassword)
        }
        return true
    }
    
    @objc func textFiedlDidChange(_ textField: UITextField){
        if textField == mainView.txtSearchBusiness{
            let arr = loginViewModel.arrBusinessList
            let arrStrBusinessList = arr.filter{ $0.businessName.lowercased().contains(textField.text!.lowercased())}
            self.businessDD.dataSource = arrStrBusinessList.map{($0.businessName)}
            self.businessDD.show()
            self.businessDD.selectionAction = { (index: Int, item: String) in
                self.mainView.txtSearchBusiness.text = item
                self.loginViewModel.strBusinessId = arrStrBusinessList[index].businessId
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- Action Zone

extension LoginVC {
    
    @IBAction func btnLoginAction(_ sender:UIButton){
        
        if mainView.txtSearchBusiness.text!.isEmpty || self.loginViewModel.strBusinessId == "" {
            makeToast(strMessage: "Please_select_business_key".localized)
        } else if mainView.txtUsername.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            makeToast(strMessage: "Please_enter_username_key".localized)
        } else if mainView.txtPassword.text!.isEmpty {
            makeToast(strMessage: "Please_enter_password_key".localized)
        } else {
            let dict = ["business_id":"\(self.loginViewModel.strBusinessId)",
                        "username":self.mainView.txtUsername.text!.trimmingCharacters(in: .whitespaces),
                        "password":self.mainView.txtPassword.text!.trimmingCharacters(in: .whitespaces),
                        "lang":lang,
                        "device_token":UUID().uuidString,
                        "register_id":"123",
                        "device_type":strDeviceType
            ]
            loginViewModel.postLoginData(param: dict) { (flg, userData,data) in
                
                guard let rowdata = try? data.rawData() else {return}
                Defaults.setValue(rowdata, forKey: "userDetails")
                Defaults.synchronize()
                
                let obj = objStoryboard.instantiateViewController(withIdentifier: "HomeParentVC") as! HomeParentVC
                self.navigateUserWithLG(obj: obj)
            }
            
        }
    }
}
