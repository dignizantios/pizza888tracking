//
//  LoginView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 23/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class LoginView: UIView {

   //MARK:- Outlet Zone
    
    @IBOutlet weak var lblSearchTitle: UILabel!
    @IBOutlet weak var txtSearchBusiness: CustomTextField!
    @IBOutlet weak var lblUsernameTitle: UILabel!
    @IBOutlet weak var txtUsername: CustomTextField!
    @IBOutlet weak var lblPasswordTitle: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    
    //MARK:- SetupUI
    
    func setupUI(theDelegate:LoginVC){
        
        lblSearchTitle.text = "Search_business_key".localized.localizedUppercase
        txtSearchBusiness.placeholder = "Search_business_key".localized.localizedUppercase
        
        lblUsernameTitle.text = "Username_key".localized.localizedUppercase
        txtUsername.placeholder = "Username_key".localized.localizedUppercase
        
        lblPasswordTitle.text = "Password_key".localized.localizedUppercase
        txtPassword.placeholder = "******".localized.localizedUppercase
        
        unSelectedSetup()
        
        [btnLoginOutlet].forEach { (button) in
            button?.layer.cornerRadius = (button?.frame.size.height)! / 2
            button?.layer.masksToBounds = true
            button?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        }
        btnLoginOutlet.backgroundColor = UIColor.appThemeRedColor
        btnLoginOutlet.setTitleColor(UIColor.white, for: .normal)
        btnLoginOutlet.setTitle("Login_key".localized, for: .normal)
        
        [txtSearchBusiness,txtUsername,txtPassword].forEach { (textfield) in
            textfield?.delegate = theDelegate
            textfield?.tintColor = .appThemeRedColor
        }
        
    }
    
    func selectedSetup(label:UILabel,textfield:CustomTextField){
        label.textColor = .appThemeRedColor
        
        textfield.textColor = .appThemeRedColor
        textfield.placeHolderColor = .appThemeRedColor
        textfield.borderColor = .appThemeRedColor
    }
    
    func unSelectedSetup(){
        [lblSearchTitle,lblUsernameTitle,lblPasswordTitle].forEach { (label) in
            label?.font = themeFont(size: 17, fontname: .regular)
            label?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [txtSearchBusiness,txtUsername,txtPassword].forEach { (textfield) in
            textfield?.font = themeFont(size: 17, fontname: .regular)
            textfield?.textColor = UIColor.appThemeLightGrayColor
            textfield?.textAlignment = .center
            textfield?.borderColor = .appThemeLightGrayColor
            textfield?.placeHolderColor = .appThemeLightGrayColor
        }
    }
    
}
