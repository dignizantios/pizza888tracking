import Foundation 
import ObjectMapper 

class LoginModel: Mappable { 

	var flag: Int = 0
	var msg: String = ""
	var loginUserData = loginData()

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		loginUserData <- map["data"]
	}
} 

class loginData: Mappable {

	var adminId: String = ""
	var businessId: String = ""
	var branchId: String = ""
	var username: String = ""
	var password: String = ""
	var emailid: String = ""
	var contactNum: String = ""
	var userType: String = ""
	var createDt: String = ""
	var logSt: String = ""
	var userPermission: Any? 
	var accessToken: String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		adminId <- map["admin_id"] 
		businessId <- map["business_id"] 
		branchId <- map["branch_id"] 
		username <- map["username"] 
		password <- map["password"] 
		emailid <- map["emailid"] 
		contactNum <- map["contact_num"] 
		userType <- map["user_type"] 
		createDt <- map["create_dt"] 
		logSt <- map["log_st"] 
		userPermission <- map["user_permission"] 
		accessToken <- map["access_token"] 
	}
} 

