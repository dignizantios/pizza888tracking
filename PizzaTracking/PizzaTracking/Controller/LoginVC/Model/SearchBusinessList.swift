import Foundation 
import ObjectMapper 

class SearchBusinessList: Mappable { 

    var msg : String = ""
	var data: [SearchBusiness] = []

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class SearchBusiness: Mappable {

	var businessId: String = ""
	var businessName: String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		businessId <- map["business_id"] 
		businessName <- map["business_name"] 
	}
} 

