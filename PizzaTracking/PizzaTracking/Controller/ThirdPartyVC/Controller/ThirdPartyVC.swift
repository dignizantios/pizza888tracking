//
//  ThirdPartyVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import DropDown

class ThirdPartyVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: ThirdPartyView = { [unowned self] in
            return self.view as! ThirdPartyView
        }()
       
    let model = ThirdPartyViewModel()
    var thirdPartyDD = DropDown()
       
       //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.setupUI(theDelegate: self)
        addDoneButtonOnKeyboard(textfield: mainView.txtOrderId)
        addDoneButtonOnKeyboard(textfield: mainView.txtAmount)
        mainView.txtThirdPartyName.addTarget(self, action: #selector(textFiedlDidChange(_:)), for: .editingChanged)
        APICalling()
    }
    
    override func viewDidLayoutSubviews() {
        self.configureDropdown(dropdown: thirdPartyDD, sender: mainView.txtThirdPartyName)
    }
    
    override func viewWillAppear(_ animated: Bool) {       
        setUpNavigationBarWithTitle(strTitle: "Third_party_key".localized.capitalized)
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu"), style: .plain, target: self, action: #selector(showLeftViewAnimated(_:)))
        leftButton.tintColor = UIColor.appThemeRedColor
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
    }
    
    func APICalling(){
        model.getThirdPartyListData(keyword: "") { (list) in
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                self.model.arrThirdPartyList = list.data
            }
        }
    }

}

//MARK:- Action Zone
extension ThirdPartyVC {
    
    @IBAction func btnPaidAction(_ sender:UIButton){
        self.view.endEditing(true)
        mainView.btnPaidSelection()
    }
    
    @IBAction func btnUnPaidAction(_ sender:UIButton){
        self.view.endEditing(true)
        mainView.btnUnPaidSelection()
    }
    
    @IBAction func btnSubmitAction(_ sender:UIButton){
        if mainView.txtOrderId.text!.isEmpty {
            makeToast(strMessage: "Please_enter_order_id_key".localized)
        } else if mainView.txtThirdPartyName.text!.isEmpty || self.model.strThirdpartyId == "" {
            makeToast(strMessage: "Please_select_third_party_key".localized)
        } else if mainView.txtCurrentAddress.text!.isEmpty {
            makeToast(strMessage: "Please_enter_current_address_key".localized)
        } else if mainView.txtDelivryAddress.text!.isEmpty {
            makeToast(strMessage: "Please_enter_delivery_address_key".localized)
        } else if !mainView.btnPaidOutlet.isSelected && !mainView.btnUnPaidOutlet.isSelected {
            makeToast(strMessage: "Please_select_payment_option_key".localized)
        } else if mainView.txtAmount.text!.isEmpty {
            makeToast(strMessage: "Please_enter_amount_key".localized)
        } else {
            self.view.endEditing(true)
            let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                     "order_id":self.mainView.txtOrderId.text ?? "",
                                     "partner_id":self.model.strThirdpartyId,
                                     "current_address":self.mainView.txtCurrentAddress.text ?? "",
                                     "delivery_address":self.mainView.txtDelivryAddress.text ?? "",
                                     "payment_type":self.mainView.btnPaidOutlet.isSelected ? "1" : "2",
                                     "driver_id":getUserDetail("driver_id"),
                                     "access_token":getUserDetail("access_token"),
                                     "amount":self.mainView.txtAmount.text!]
            model.addThirdParty(param: dict) { (data) in
                if data.flag  == Int(strAccessDenied){
                    self.logoutAPICalling()
                } else if data.flag == Int(strSuccessResponse) {
                    self.mainView.removeAllTextFieldValue()
                    makeToast(strMessage: data.msg)
                }
            }            
        }
    }
}

//MARK:- TextFiedl Delegate

extension ThirdPartyVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mainView.txtThirdPartyName{
            self.thirdPartyDD.dataSource = model.arrThirdPartyList.map{($0.partnerName)}
            self.thirdPartyDD.show()
            self.thirdPartyDD.selectionAction = { (index: Int, item: String) in
                self.mainView.txtThirdPartyName.text = item
                self.model.strThirdpartyId = self.model.arrThirdPartyList[index].partnerId
            }
        }
        return true
    }
       
       @objc func textFiedlDidChange(_ textField: UITextField){
           if textField == mainView.txtThirdPartyName{
               let arr = model.arrThirdPartyList
               let arrStrThirdPartyList = arr.filter{ $0.partnerName.lowercased().contains(textField.text!.lowercased())}
               self.thirdPartyDD.dataSource = arrStrThirdPartyList.map{($0.partnerName)}
               self.thirdPartyDD.show()
               self.thirdPartyDD.selectionAction = { (index: Int, item: String) in
                   self.mainView.txtThirdPartyName.text = item
                   self.model.strThirdpartyId = arrStrThirdPartyList[index].partnerId
               }
           }
       }
}
