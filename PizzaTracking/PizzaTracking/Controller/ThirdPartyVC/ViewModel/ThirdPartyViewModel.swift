//
//  ThirdPartyViewModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 03/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ThirdPartyViewModel {
    
    //MARK:- Variable Declaration
    
    var arrThirdPartyList = [ThirdPartyData]()
    var strThirdpartyId = String()
}

//MARK:- Service

extension ThirdPartyViewModel {
    func getThirdPartyListData(keyword:String,completion:@escaping(ThirdPartyListData) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kThirdPartyList)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "search_keyword" : keyword,
                         "business_id":getUserDetail("business_id")]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let thirpartyList = ThirdPartyListData(JSON: dicData) {
                            completion(thirpartyList)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let thirpartyList = ThirdPartyListData(JSON: dicData) {
                            completion(thirpartyList)
                        }
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
    
    func addThirdParty(param:[String:Any],completion:@escaping(ThirdPartyAddedData) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kAddThirdParty)"
            
            print("URL: \(url)")            
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
}
