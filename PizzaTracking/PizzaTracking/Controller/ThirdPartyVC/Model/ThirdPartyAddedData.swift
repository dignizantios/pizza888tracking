import Foundation 
import ObjectMapper 

class ThirdPartyAddedData: Mappable { 

	var flag: Int = -1
	var msg: String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
	}
} 

