import Foundation 
import ObjectMapper 

class ThirdPartyListData: Mappable { 

    var flag: Int = 0
	var msg: String = ""
	var data = [ThirdPartyData]()

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		msg <- map["msg"] 
		data <- map["data"]
        flag <- map["flag"]
	}
} 

class ThirdPartyData: Mappable {

	var partnerId: String = ""
	var partnerName: String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		partnerId <- map["partner_id"] 
		partnerName <- map["partner_name"] 
	}
} 

