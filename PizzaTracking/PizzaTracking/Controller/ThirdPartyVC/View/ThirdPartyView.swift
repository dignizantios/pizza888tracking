//
//  ThirdPartyView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class ThirdPartyView: UIView {

   //MARK:- Outlet Zone
    
    @IBOutlet weak var lblOrderIdTitle: UILabel!
    @IBOutlet weak var txtOrderId: UITextField!
    @IBOutlet weak var lblThirdPartNameTitle: UILabel!
    @IBOutlet weak var txtThirdPartyName: UITextField!
    @IBOutlet weak var lblCurrentAddressTitle: UILabel!
    @IBOutlet weak var txtCurrentAddress: UITextField!
    @IBOutlet weak var lblDelivryAddressTitle: UILabel!
    @IBOutlet weak var txtDelivryAddress: UITextField!
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var btnPaidOutlet: UIButton!
    @IBOutlet weak var btnUnPaidOutlet: UIButton!
    @IBOutlet weak var btnSubmitOutlet: CustomButton!
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    
    //MARK:- SetupUI
    
    func setupUI(theDelegate:ThirdPartyVC){
        [lblOrderIdTitle,lblThirdPartNameTitle,lblCurrentAddressTitle,lblDelivryAddressTitle,lblPaymentTitle,lblAmountTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .black
        }
        
       [txtOrderId,txtDelivryAddress,txtThirdPartyName,txtCurrentAddress,txtAmount].forEach { (txtField) in
            txtField?.font = themeFont(size: 16, fontname: .medium)
            txtField?.textColor = .black
        txtField?.placeholder = "Enter_here_key".localized
        txtField?.delegate = theDelegate
        }
        txtThirdPartyName.placeholder = "Search_here_key".localized
        
        lblOrderIdTitle.text = "Order_id_key".localized
        lblThirdPartNameTitle.text = "Third_party_name_key".localized
        lblCurrentAddressTitle.text = "Current_address_key".localized
        lblDelivryAddressTitle.text = "Delivery_address_key".localized
        lblPaymentTitle.text = "Payment_key".localized
        lblAmountTitle.text = "Amount_key".localized
        
        [btnSubmitOutlet].forEach { (button) in
            button?.layer.cornerRadius = (button?.frame.size.height)! / 2
            button?.layer.masksToBounds = true
            button?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        }
        btnSubmitOutlet.backgroundColor = UIColor.appThemeRedColor
        btnSubmitOutlet.setTitleColor(UIColor.white, for: .normal)
        btnSubmitOutlet.setTitle("Submit_key".localized, for: .normal)
        
        [btnPaidOutlet,btnUnPaidOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.setTitleColor(.appThemeBlackColor, for: .normal)
        }
        
        btnPaidOutlet.setTitle("Paid_key".localized, for: .normal)
        btnUnPaidOutlet.setTitle("Unpaid_key".localized, for: .normal)
        
    }
    
    func btnPaidSelection(){
        btnPaidOutlet.isSelected = true
        btnUnPaidOutlet.isSelected = false
    }
    
    func btnUnPaidSelection(){
        btnPaidOutlet.isSelected = false
        btnUnPaidOutlet.isSelected = true
    }
    
    func removeAllTextFieldValue(){
        [txtOrderId,txtDelivryAddress,txtThirdPartyName,txtCurrentAddress,txtAmount].forEach { (txtField) in
            txtField?.text = ""
        }
    }

}
