import Foundation 
import ObjectMapper 

class ThirdPartyJobListModel: Mappable {

	var nextOffset: String = ""
	var flag: Int = -1
	var msg: String = ""
	var data = [ThirdPartyJobListData]()

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		nextOffset <- map["next_offset"] 
		flag <- map["flag"] 
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class ThirdPartyJobListData: Mappable {

	var ordid: String = ""
    var businessId: String = ""
    var orderId: String = ""
    var partnerId: String = ""
    var currentAddress: String = ""
    var currentLat: String = ""
    var currentLong: String = ""
    var deliveryAddress: String = ""
    var deliveryLat: String = ""
    var deliveryLong: String = ""
    var paymentType: String = ""
    var orderStatus: String = ""
    var createdDatetime: String = ""
    var updateDatetime: String = ""
    var partnerName: String = ""
    var orderPrice: String = ""
    var paymentFlag: String = ""
    var orderTrackId:String = ""
    var polyline : String = ""
    var paymentStatus:String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
        ordid <- map["ordid"]
        businessId <- map["business_id"]
        orderId <- map["order_id"]
        partnerId <- map["partner_id"]
        currentAddress <- map["current_address"]
        currentLat <- map["current_lat"]
        currentLong <- map["current_long"]
        deliveryAddress <- map["delivery_address"]
        deliveryLat <- map["delivery_lat"]
        deliveryLong <- map["delivery_long"]
        paymentType <- map["payment_type"]
        orderStatus <- map["order_status"]
        createdDatetime <- map["created_datetime"]
        updateDatetime <- map["update_datetime"]
        partnerName <- map["partner_name"]
        orderPrice <- map["order_price"]
        paymentFlag <- map["payment_flag"]
        orderTrackId <- map["order_track_id"]
        polyline <- map["polyline"]
        paymentStatus <- map["payment_status"]
    }
} 

