//
//  ThirdPartyListVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 02/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class ThirdPartyListVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: ThirdPartyListView = { [unowned self] in
        return self.view as! ThirdPartyListView
        }()
       
    var upperReferesh = UIRefreshControl()
    
    let theModel = ThirdPartyListViewModel()
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setupUI(theDelegate: self)
        setupUI()
        if theModel.selectMainParentController == .home {
            APICallingFromHome()
        } else {
           APICallingFromMyTrip()
        }
    }
    
    func setupModelData(type:selectJobType,parentType:selectionParentControlFromSidemenu){
        theModel.selectedParentCotroller = type
        theModel.selectMainParentController = parentType
    }
    
    func APICallingFromHome(isLoaderShow:Bool = true){
        if isLoaderShow {
            showLoader()
            theModel.intOffset = 0
        }
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(theModel.intOffset)",
                    "flag":"\(theModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token")]
        
        theModel.getJobListData(kURL: kJobs, param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                if self.theModel.intOffset == 0 || self.theModel.intOffset == -1{
                    self.theModel.arrJobList = []
                }
                self.theModel.dictThirPartyList = list
                self.theModel.arrJobList += list.data
                self.mainView.tblThirdParty.reloadData()
            } else {
                self.theModel.dictThirPartyList = list
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
    
    @objc func APICallForHome(){
        APICallingFromHome()
    }
    
    func APICallingFromMyTrip(isLoaderShow:Bool = true){
        if isLoaderShow {
            showLoader()
            theModel.intOffset = 0
        }
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(theModel.intOffset)",
                    "flag":"\(theModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token")]
        
        theModel.getJobListData(kURL: kMyTrip, param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse){
                if self.theModel.intOffset == 0 || self.theModel.intOffset == -1{
                    self.theModel.arrJobList = []
                }
                self.theModel.dictThirPartyList = list
                self.theModel.arrJobList += list.data
                self.mainView.tblThirdParty.reloadData()
            } else {
                self.theModel.dictThirPartyList = list
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
    
    @objc func APICallForTrip(){
        APICallingFromMyTrip()
    }
    
    func setupUI(){
        if theModel.selectMainParentController == .home {
            upperReferesh.addTarget(self, action: #selector(APICallForHome), for: .valueChanged)
        } else {
           upperReferesh.addTarget(self, action: #selector(APICallForTrip), for: .valueChanged)
        }
        mainView.tblThirdParty.addSubview(upperReferesh)
        mainView.tblThirdParty.tableFooterView = UIView()
    }
}

//MARK:- Action Zone

extension ThirdPartyListVC {
    
    @IBAction func btnCardAction(_ sender:UIButton){
        let dict = theModel.arrJobList[sender.tag]
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(theModel.intOffset)",
                    "flag":"\(theModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token"),
                    "ordid":dict.ordid,
                    "payment_status":selectedPaymentFlag.card.rawValue,
                    "amount":dict.orderPrice]
        
        theModel.orderPayment(param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                self.updateList(dict: list.data[0])
            } else {
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
    
    @IBAction func btnCashAction(_ sender:UIButton){
        let dict = theModel.arrJobList[sender.tag]
        let param = ["lang" : lang,
                    "business_id" : getUserDetail("business_id"),
                    "offset":"\(theModel.intOffset)",
                    "flag":"\(theModel.selectedParentCotroller.rawValue)",
                    "driver_id":getUserDetail("driver_id"),
                    "access_token":getUserDetail("access_token"),
                    "ordid":dict.ordid,
                    "payment_status":selectedPaymentFlag.cash.rawValue,
                    "amount":dict.orderPrice]
        
        theModel.orderPayment(param: param) { (list) in
            self.upperReferesh.endRefreshing()
            if list.flag == Int(strAccessDenied) {
                self.logoutAPICalling()
            } else if list.flag == Int(strSuccessResponse) {
                self.updateList(dict: list.data[0])
            } else {
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
    
    func updateList(dict:ThirdPartyJobListData){
        if let index = theModel.arrJobList.firstIndex(where: {$0.ordid == dict.ordid}) {
            theModel.arrJobList[index] = dict
            UIView.performWithoutAnimation {
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
    
    func removeList(dict:ThirdPartyJobListData){
        if let index = theModel.arrJobList.firstIndex(where: {$0.ordid == dict.ordid}) {
            theModel.arrJobList.remove(at: index)
            UIView.performWithoutAnimation {
                self.mainView.tblThirdParty.reloadData()
            }
        }
    }
}

//MARK:- TableViewDelegate

extension ThirdPartyListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if theModel.arrJobList.count == 0 {
            let lbl = UILabel()
            lbl.text = theModel.dictThirPartyList.msg
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return theModel.arrJobList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        [cell.viewName,cell.viewOrderStore].forEach { (view) in
            view?.isHidden = true
        }
        let dict = theModel.arrJobList[indexPath.row]
        cell.lblOrderId.text = "\("Order_id_key".localized) : \(dict.orderId)"
        cell.lblAddress.text = "\(dict.deliveryAddress)"
        
        cell.lblAmount.text = "$ \(dict.orderPrice)"
        cell.btnStartOutlet.tag = indexPath.row
        cell.btnStartOutlet.addTarget(self, action: #selector(btnStartAction(_:)), for: .touchUpInside)
        if theModel.selectMainParentController == .mytrip {
            cell.viewStart.isHidden = true
        }
        
        cell.btnCashOutlet.tag = indexPath.row
        cell.btnCashOutlet.addTarget(self, action: #selector(btnCashAction(_:)), for: .touchUpInside)
        
        cell.btnCardOutlet.tag = indexPath.row
        cell.btnCardOutlet.addTarget(self, action: #selector(btnCardAction(_:)), for: .touchUpInside)
        
        if dict.paymentFlag == "1" {
            cell.vwPaymentFlag.isHidden = true
            cell.vwPaymentType.isHidden = false
            cell.lblPaymentName.text = dict.paymentStatus == "1" ? "Cash_key".localized : "Card_key".localized
            cell.viewStart.isHidden = false
        } else {
            cell.vwPaymentFlag.isHidden = false
            cell.vwPaymentType.isHidden = true
            cell.viewStart.isHidden = true
        }
        
        if theModel.selectMainParentController == .mytrip {
            cell.viewStart.isHidden = true
            cell.vwPaymentFlag.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theModel.selectMainParentController == .mytrip {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyTripVC") as! MyTripVC
            obj.setupDataThirdParty(theModel: theModel.arrJobList[indexPath.row], type: theModel.selectMainParentController, jobType: theModel.selectedParentCotroller)
            obj.handlerThirdPartyUpdateList = {[weak self] dict,isCancel in
                if isCancel {
                    self?.removeList(dict: dict)
                } else {
                    self?.updateList(dict: dict)
                }
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theModel.intOffset != -1 && theModel.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    @objc func btnStartAction(_ sender:UIButton){
        let dictData = theModel.arrJobList[sender.tag]
        let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                "ordid":dictData.ordid,
                                "driver_id":getUserDetail("driver_id"),
                                "access_token":getUserDetail("access_token"),
                                "flag":theModel.selectedParentCotroller.rawValue,
                                "lang":lang]
        theModel.startJob(param: dict) { (data) in
            if data.flag  == Int(strAccessDenied){
                self.logoutAPICalling()
            } else if data.flag == Int(strSuccessResponse) {
                makeToast(strMessage: data.msg)
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyTripVC") as! MyTripVC
                var dictData = data.data[0]
                obj.setupDataThirdParty(theModel: data.data[0], type: .mytrip, jobType: self.theModel.selectedParentCotroller)
                if self.theModel.selectMainParentController == .mytrip {
                    obj.handlerThirdPartyUpdateList = { [weak self] dict,isCancel in
                        if isCancel {
                           self?.updateList(dict: dict)
                        } else {
                            self?.removeList(dict: dict)
                        }
                    }
                } else {
                    self.removeList(dict: data.data[0])
                }
                self.navigationController?.pushViewController(obj, animated: true)
                self.toRedirectGoogleMaps(model: data.data[0])
            }
        }
    }
    
    func toRedirectGoogleMaps(model:ThirdPartyJobListData){    
        if let url = URL(string: "comgooglemaps://?saddr=\(model.currentLat),\(model.currentLong)&daddr=\(model.deliveryLat),\(model.deliveryLong)&directionsmode=driving") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}

//MARK:- Scrollview delegate

extension ThirdPartyListVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if theModel.intOffset != -1 && theModel.intOffset != 0 {
            if (Double(mainView.tblThirdParty.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(mainView.tblThirdParty.contentSize.height).rounded(toPlaces: 2)) - (Double(mainView.tblThirdParty.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                if theModel.selectMainParentController == .home {
                    APICallingFromHome(isLoaderShow: false)
                } else {
                   APICallingFromMyTrip(isLoaderShow: false)
                }
            }
        }
    }
}

