//
//  ThirdPartyListView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 02/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class ThirdPartyListView: UIView {
    
    //MARK:- Outlet Zone

    @IBOutlet weak var tblThirdParty: UITableView!    
   
    
    //MARK:- ViewLife Cycle
    
    func setupUI(theDelegate:ThirdPartyListVC){
        
        tblThirdParty.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        
        
    }

}
