//
//  CancelTripView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 03/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class CancelTripView: UIView {

    //MARK:- Outlet Zone
    @IBOutlet weak var txtComment: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var btnSubmitOutlet: CustomButton!
    
    //MARK:- Setup UI
    
    func setupUI(theDelegate:CancelTripVC){
        
        [btnSubmitOutlet].forEach { (button) in
            button?.layer.cornerRadius = (button?.frame.size.height)! / 2
            button?.layer.masksToBounds = true
            button?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        }
        btnSubmitOutlet.backgroundColor = UIColor.appThemeRedColor
        btnSubmitOutlet.setTitleColor(UIColor.white, for: .normal)
        btnSubmitOutlet.setTitle("Submit_key".localized, for: .normal)
        
        lblPlaceholder.font = themeFont(size: 15, fontname: .medium)
        lblPlaceholder.textColor = UIColor.black
        lblPlaceholder.text = "Enter_here_key".localized
        
        txtComment.contentInset = UIEdgeInsets(top: 4, left: 3, bottom: 3, right: 8)
        txtComment.font = themeFont(size: 15, fontname: .medium)
        txtComment.textColor = UIColor.black
        txtComment.borderColor = UIColor.appThemeRedColor
        txtComment.delegate = theDelegate
        txtComment.tintColor = .appThemeBlackColor
    }

}
