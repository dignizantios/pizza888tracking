//
//  CancelTripVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 03/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class CancelTripVC: UIViewController {
    
    //MARK: Variables
    
    lazy var mainView: CancelTripView = { [unowned self] in
        return self.view as! CancelTripView
        }()
    
    let model = CancelTripViewModel()
    var handlerUpdateList:() -> Void = {}
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setupUI(theDelegate: self)
    }
    
    func setupDataJobData(theModel:JobModelData,type:selectJobType){
        model.dictJobData = theModel
        model.selectedParentCotroller = type
    }
    
    func setupDataThirdPartyData(theModel:ThirdPartyJobListData,jobType:selectJobType) {
        model.dictThirdPartyJobData = theModel
        model.selectedParentCotroller = jobType
    }
}

//MARK:- Textview Delegate

extension CancelTripVC:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        mainView.lblPlaceholder.isHidden = mainView.txtComment.text.isEmpty ? false : true
    }
}

//MARK:- Action Zone

extension CancelTripVC {
    
    @IBAction func btnSubmitAction(_ sender:UIButton){
        if mainView.txtComment.text.trimmingCharacters(in: .whitespaces).isEmpty {
            makeToast(strMessage: "Please_enter_reason_key".localized)
        } else {
            self.view.endEditing(true)
            let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                     "ordid":model.selectedParentCotroller == .simpleJob ? model.dictJobData.ordid : model.dictThirdPartyJobData.ordid,
                                     "driver_id":getUserDetail("driver_id"),
                                     "access_token":getUserDetail("access_token"),
                                     "reason":mainView.txtComment.text ?? "",
                                     "flag":model.selectedParentCotroller.rawValue,
                                     "lang":lang,
                                     "order_track_id":model.selectedParentCotroller == .simpleJob ? model.dictJobData.orderTrackId : model.dictThirdPartyJobData.orderTrackId]
            model.cancelTripParty(param: dict) { (data) in
                if data.flag  == Int(strAccessDenied){
                    self.logoutAPICalling()
                } else if data.flag == Int(strSuccessResponse){
                    self.handlerUpdateList()
                    makeToast(strMessage: data.msg)
                    self.dismiss(animated: false, completion: nil)
                }
            }
        }
    }
    
    @IBAction func btnCloseAction(_ sender:UIButton){
        self.dismiss(animated: false, completion: nil)
    }
}
