//
//  CancelTripViewModel.swift
//  PizzaTracking
//
//  Created by Jaydeep on 03/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class CancelTripViewModel {
    
    //MARK:- Variable Declaration
    
    var dictJobData = JobModelData()
    var selectedParentCotroller = selectJobType.simpleJob
    var dictThirdPartyJobData = ThirdPartyJobListData()
    
    //MARK:- Service
    
    func cancelTripParty(param:[String:Any],completion:@escaping(ThirdPartyAddedData) -> Void){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(kBaseURL)\(kCancelTrip)"
            
            print("URL: \(url)")
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied {
                        if let dicData =  json.dictionaryObject , let thirdPartyAdded = ThirdPartyAddedData(JSON: dicData) {
                            completion(thirdPartyAdded)
                        }
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
    }
}





