//
//  SideMenuView.swift
//  PizzaTracking
//
//  Created by Jaydeep on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class SideMenuView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblSideMenu: UITableView!
    @IBOutlet weak var lblHelloTitle: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    //MARK:- Setup View
    
    func setupUI(theDelegate:SideMenuVC){
        
        lblHelloTitle.text = "\("Hello_key".localized),"
        lblHelloTitle.font = themeFont(size: 17, fontname: .regular)
        lblHelloTitle.textColor = .white
      
        lblUsername.font = themeFont(size: 17, fontname: .semibold)
        lblUsername.textColor = .white
        
        tblSideMenu.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
        
    }
    

}
