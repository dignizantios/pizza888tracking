//
//  SideMenuVC.swift
//  PizzaTracking
//
//  Created by Jaydeep on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class SideMenuVC: UIViewController {
        
    //MARK: Variables
    lazy var mainView: SideMenuView = { [unowned self] in
        return self.view as! SideMenuView
        }()
    var arrSideMenu:[JSON] = []
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.setupUI(theDelegate: self)
        setupSideMenuArray()
        
        mainView.lblUsername.text = getUserDetail("username")
    }
    
    func setupSideMenuArray(){
        
        arrSideMenu = []
        
        var dict = JSON()
        dict["name"].stringValue = "Home_key".localized
        dict["image"].stringValue = "ic_my_home"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "My_trip_key".localized
        dict["image"].stringValue = "ic_my_trip"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Third_party_key".localized
        dict["image"].stringValue = "ic_third_party"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Logout_key".localized
        dict["image"].stringValue = "ic_logout"
        arrSideMenu.append(dict)
        
        self.mainView.tblSideMenu.reloadData()
    }

}

//MARK:- Tablview Delegate&Datasource

extension SideMenuVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        let dict = arrSideMenu[indexPath.row]
        cell.lblTitle.text = dict["name"].stringValue.localizedCapitalized
        cell.btnImageOutlet.setImage(UIImage(named: dict["image"].stringValue), for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrSideMenu[indexPath.row]
        if dict["name"].stringValue.localized == "Home_key".localized {
           let obj = objStoryboard.instantiateViewController(withIdentifier: "HomeParentVC") as! HomeParentVC
            obj.setupData(selecedVC: .home)
            self.navigateUserWithLG(obj: obj)
        } else if dict["name"].stringValue.localized == "My_trip_key".localized {
           let obj = objStoryboard.instantiateViewController(withIdentifier: "HomeParentVC") as! HomeParentVC
            obj.setupData(selecedVC: .mytrip)
            self.navigateUserWithLG(obj: obj)
        } else if dict["name"].stringValue.localized == "Third_party_key".localized {
           let obj = objStoryboard.instantiateViewController(withIdentifier: "ThirdPartyVC") as! ThirdPartyVC            
            self.navigateUserWithLG(obj: obj)
        } else if dict["name"].stringValue.localized == "Logout_key".localized {
            
            let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_logout?_key".localized, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                
                self.logoutAPICalling()
                
            }
            let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
}
