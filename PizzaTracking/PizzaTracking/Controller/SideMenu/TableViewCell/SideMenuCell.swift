//
//  SideMenuCell.swift
//  PizzaTracking
//
//  Created by Jaydeep on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnImageOutlet: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
