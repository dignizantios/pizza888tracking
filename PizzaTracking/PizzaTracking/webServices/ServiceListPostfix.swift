//
//  ServiceListPostfix.swift
//  PizzaTracking
//
//  Created by Jaydeeo on 30/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation

let kBaseURL = "https://tech88.com.au/dignizant/pizza-tracking/Api/Pizza_track/"

//https://tech88.com.au/dignizant/pizza-tracking/Api/pizza_track/login

//let kBaseURL = "http://192.168.0.136/pizza-tracking/Api/Pizza_track/"

let kSearchBusinessList = "business_search_list"

let kLogin = "login"

let kJobs = "jobs"

let kMyTrip = "mytrip"

let kLogout = "logout"

let kThirdPartyList = "thirtparty_name"

let kAddThirdParty = "add_thirdparty_order"

let kCancelTrip = "cancel_job"

let kStartJob = "start_job"

let kOrderPayment = "order_payment"

let kFinishJob = "finish_job"


