//
//  Service.swift
//  PizzaTracking
//
//  Created by Jaydeeo on 30/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService {
    
    func Service(url:String,param : [String:Any],isShowLoader:Bool=true,completion:@escaping(Result<JSON>)-> ())
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            if isShowLoader{
                showLoader()
            }
            
            print("url - ",url)
            print("param - ",param)
            
            Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
                {
                    if isShowLoader{
                        hideLoader()
                    }
                    
                    if $0.result.isSuccess
                    {
                        completion($0.result)
                    }
                    else if $0.result.isFailure
                    {
                        let statusCode = $0.response?.statusCode
                        print("StatusCode : \(statusCode)")
                        let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                        print("API result str - ",str)
                        
                        if(statusCode == 500)
                        {
                            
                        }
                        else if(statusCode != nil)
                        {
                            /// APIResponseHandle(statusCode: statusCode!)
                            completion($0.result)
                        }
                        else
                        {
                            
                            makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                            completion($0.result)
                        }
                    }
                    else
                    {
                        makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                        //                    completion($0.result)
                    }
            })
        } else {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
        
    }
    
    func uploadImagesService(url:String,img:UIImage,withName:String,fileName:String,isPdf:Bool,pdfData:Data,param : [String:Any],completion:@escaping(Result<JSON>)-> ())
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            showLoader()
            
            print("url - ",url)
            
            let unit64:UInt64 = 10_000_000
            
            let PasswordString =  String(format: "\(basic_username):\(basic_password)")
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in                
                if isPdf {
                    multipartFormData.append(pdfData, withName: withName, fileName:fileName , mimeType: "application/pdf")
                } else {
                    if let imgData = img.jpegData(compressionQuality: 0.7) {
                        multipartFormData.append(imgData, withName: withName, fileName:fileName , mimeType: "image/png")
                    }
                }
                
                for (key, value) in param {
                    multipartFormData.append((value as! NSString).data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: key)
                }
                
            }, usingThreshold: unit64, to: url, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler:
                        {
                            
                            print("Result : \($0.result)")
                            
                            if $0.result.isSuccess
                            {
                                completion($0.result)
                            }
                            else if $0.result.isFailure
                            {
                                let statusCode = $0.response?.statusCode
                                print("StatusCode : \(statusCode)")
                                if(statusCode == 500)
                                {
                                    
                                }else if(statusCode != nil)
                                {
                                    completion($0.result)
                                }
                                else
                                {
                                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                                }
                            }else
                            {
                                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                            }
                    })
                case .failure(_):
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            })
            
        }
        else {
            
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
        
        
    }
 
}

