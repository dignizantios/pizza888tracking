//
//  AppDelegate.swift
//  PizzaTracking
//
//  Created by Jaydeep on 23/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseDatabase
import Polyline
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK:- Varialbe Declaration
    
    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    var locationManager = CLLocationManager()
    var lattitude  = Double()
    var longitude = Double()
    var handlerLocationUpdate:(Double,Double,String) -> Void = {_,_,_ in}
    var arrWayPoint: [CLLocationCoordinate2D] = []
    var ref =  DatabaseReference()
    var handlerBackToMainView:() -> Void = {}
    
    //MARK:- Appdelegate Method

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        ref = Database.database().reference()
        GMSServices.provideAPIKey(kGoogleMapsKey)
//        setUpQuickLocationUpdate()
        return true
    }
}

//MARK:- Location Manager

extension AppDelegate:CLLocationManagerDelegate {
    
    func setUpQuickLocationUpdate() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 20
        locationManager.activityType = .automotiveNavigation
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let latestLocation = locations.first
        {

        
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                lattitude = latestLocation.coordinate.latitude
                longitude = latestLocation.coordinate.longitude
//                print("lattitude:- \(lattitude), longitude:- \(longitude)")
                
                if let isTripStarted = Defaults.value(forKey: "isTripStarted") as? String {
//                    print("isTripStarted \(isTripStarted)")
                    let userID : String = getUserDetail("driver_id")
                    var wayPoint:[String:String] = [:]
                    wayPoint = ["way_lat": String(lattitude),
                                "way_lng": String(longitude)]
                    
                    let destination = CLLocationCoordinate2DMake(lattitude, longitude)
                    arrWayPoint.append(destination)
//                    print("arrWayPoint \(arrWayPoint)")
                    
                    let polyline = Polyline(coordinates: arrWayPoint)
                    let encodedPolyline: String = polyline.encodedPolyline
                    print("encodedPolyline \(encodedPolyline)")
                            
                    handlerLocationUpdate(lattitude,longitude,encodedPolyline)
                    
                    var dict = [String:Any]()
                    dict["lat"] = lattitude
                    dict["long"] = longitude
                    dict["polyline"] = encodedPolyline
                    ref.child("user-\(userID)").updateChildValues(dict)
                    
                    let userDeliverLatLong = CLLocation(latitude: kDeliverLatitude, longitude: kDeliverLongitude)
                    
                    let userCurrentLatLong = CLLocation(latitude: kDeliverLatitude, longitude: kDeliverLongitude)
                    
                    let distance = userCurrentLatLong.distance(from: userDeliverLatLong)
                    
                    /*if distance <= 20 {
                        automaticFinishCallAPI(polyline: encodedPolyline)
                    }*/
                } else {
                    locationManager.stopUpdatingLocation()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSetting()
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error :- \(error)")
    }
    
    //MARK:- open Setting
    
    func openSetting() {
        let alertController = UIAlertController (title: "PizzaTracking".localized, message: "Go_to_settings_key".localized, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Setting_key", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}

//MARK:-  API()
extension AppDelegate {
    func automaticFinishCallAPI(polyline:String){
        let model = MyTripViewModel()
        
        let dict:[String:Any] = ["business_id":getUserDetail("business_id"),
                                 "ordid":kOrdId,
                                 "finish_flag":"1",
                                 "driver_id":getUserDetail("driver_id"),
                                 "access_token":getUserDetail("access_token"),
                                 "lang":lang,
                                 "flag":kJobFlag,
                                 "order_track_id":kOrderTrackId,
                                 "polyline":polyline]
        
        model.finishJob(param: dict) { (data) in
            if data.flag  == Int(strAccessDenied){
                self.window?.rootViewController?.logoutAPICalling()
            } else if data.flag == Int(strSuccessResponse) {
                self.ref.child("user-\(getUserDetail("driver_id"))").removeValue()
                self.locationManager.stopUpdatingLocation()
                Defaults.removeObject(forKey: "isTripStarted")
                Defaults.synchronize()
                self.handlerBackToMainView()
            }
        }
    }
}
