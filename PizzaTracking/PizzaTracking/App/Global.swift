//
//  Global.swift
//  PizzaTracking
//
//  Created by Jaydeep on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import MaterialComponents
import SwiftyJSON

let objStoryboard = UIStoryboard(name: "Main", bundle: nil)

// Google Maps Key

let kGoogleMapsKey = "AIzaSyAI8Oi8hv4wq2YbAMZho9jC86wwzXlf4W0"

//MARK:- Loader

func showLoader() {
    AppDelegate.shared.window?.isUserInteractionEnabled = false
    SVProgressHUD.show() 
}

func hideLoader(){
    AppDelegate.shared.window?.isUserInteractionEnabled = true
    SVProgressHUD.dismiss()
}

var basic_username = "pizza_track"
var basic_password = "QJ$h9tXr)~d+rn+b"
var lang = "en"
let Defaults = UserDefaults.standard
let strDeviceType = "1"
let strSuccessResponse = "1"
let strAccessDenied = "-1"
let googleMapZoomLevel = 15.0
var kDeliverLatitude = Double()
var kDeliverLongitude = Double()
var kOrdId = ""
var kJobFlag = ""
var kOrderTrackId = ""

//MARK: - Set Toaster
func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)    
}

func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}
